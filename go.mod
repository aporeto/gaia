module go.aporeto.io/gaia

go 1.22

toolchain go1.22.6

// Aporeto
require go.aporeto.io/elemental v1.123.1-0.20240822212917-6f8c7be6698c

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofrs/uuid v4.4.0+incompatible // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/mitchellh/copystructure v1.2.0
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/smartystreets/assertions v1.13.0 // indirect
	github.com/smartystreets/goconvey v1.7.2
	github.com/ugorji/go/codec v1.2.8 // indirect
	github.com/yl2chen/cidranger v1.0.2
	gopkg.in/yaml.v2 v2.4.0
)

require go.mongodb.org/mongo-driver v1.16.0

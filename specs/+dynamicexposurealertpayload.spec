# Model
model:
  rest_name: dynamicexposurealertpayload
  resource_name: dynamicexposurealertpayloads
  entity_name: DynamicExposureAlertPayload
  package: vargid
  group: pcn/infrastructure
  description: Parameters related to the dynamic exposure of an asset associated with
    an alert.
  detached: true

# Attributes
attributes:
  v1:
  - name: FQDN
    description: Fully qualified domain name of the asset associated with the alert.
    type: string
    exposed: true
    stored: true

  - name: port
    description: |-
      Port number that is associated with the specific dynamically exposed public IP
      address.
    type: integer
    exposed: true
    stored: true

  - name: protocol
    description: Protocol associated with the public IP address in this alert.
    type: string
    exposed: true
    stored: true

  - name: publicIP
    description: Public IP address associated with the alert.
    type: string
    exposed: true
    stored: true

  - name: serverResponse
    description: |-
      Server response received while dynamically probing the public IP address in this
      alert.
    type: string
    exposed: true
    stored: true
